# deep-comp-vis

Python for Computer Vision with OpenCV and Deep Learning
Learn the latest techniques in computer vision with Python , OpenCV , and Deep Learning!
Course - Created by Jose Portilla

# Description
Welcome to the ultimate online course on Python for Computer Vision!

This course is your best resource for learning how to use the Python programming language for Computer Vision.

We'll be exploring how to use Python and the OpenCV (Open Computer Vision) library to analyze images and video data.

The most popular platforms in the world are generating never before seen amounts of image and video data. Every 60 seconds users upload more than 300 hours of video to Youtube, Netflix subscribers stream over 80,000 hours of video, and Instagram users like over 2 million photos! Now more than ever its necessary for developers to gain the necessary skills to work with image and video data using computer vision.

Computer vision allows us to analyze and leverage image and video data, with applications in a variety of industries, including self-driving cars, social network apps, medical diagnostics, and many more.

As the fastest growing language in popularity, Python is well suited to leverage the power of existing computer vision libraries to learn from all this image and video data.

In this course we'll teach you everything you need to know to become an expert in computer vision! This $20 billion dollar industry will be one of the most important job markets in the years to come.

We'll start the course by learning about numerical processing with the NumPy library and how to open and manipulate images with NumPy. Then will move on to using the OpenCV library to open and work with image basics. Then we'll start to understand how to process images and apply a variety of effects, including color mappings, blending, thresholds, gradients, and more.

Then we'll move on to understanding video basics with OpenCV, including working with streaming video from a webcam.  Afterwards we'll learn about direct video topics, such as optical flow and object detection. Including face detection and object tracking.

Then we'll move on to an entire section of the course devoted to the latest deep learning topics, including image recognition and custom image classifications. We'll even cover the latest deep learning networks, including the YOLO (you only look once) deep learning network.

This course covers all this and more, including the following topics:

* NumPy
* Images with NumPy
* Image and Video Basics with NumPy
* Color Mappings
* Blending and Pasting Images
* Image Thresholding
* Blurring and Smoothing
* Morphological Operators
* Gradients
* Histograms
* Streaming video with OpenCV
* Object Detection
* Template Matching
* Corner, Edge, and Grid Detection
* Contour Detection
* Feature Matching
* WaterShed Algorithm
* Face Detection
* Object Tracking
* Optical Flow
* Deep Learning with Keras
* Keras and Convolutional Networks
* Customized Deep Learning Networks
* State of the Art YOLO Networks


and much more!

Feel free to message me on Udemy if you have any questions about the course!

Thanks for checking out the course page, and I hope to see you inside!

Jose

Who this course is for:
Python Developers interested in Computer Vision and Deep Learning. This course is not for complete python beginners.